<?php
namespace Core\Controllers;

use \Core\Models\Helper as Helper;
use \Core\Models as Model;

Class RenderController extends \Core\Models\Render {

	public $url;
	protected $node;
	public $template;
	
	protected $header;
	protected $footer;
	protected $navigation;
	
	protected $css;
	protected $js;
	
	public function __construct($url,$node,$css,$js){
		$this->url = $url;
		$this->node = $node;
		$this->css = $css;
		$this->js = $js;
		
		if(!$this->node){
			$this->template = NONE;
		};
		if($this->node==PAGE){
			if((count($this->url)==1)&&strlen($this->url[0])==0){
				$this->template = HOME;
			} elseif (file_exists(VIEW.$this->url[0].'.'.EXT)) {
				$this->template = $this->url[0];
			} else {
				$this->template = DEF;
			}
		} elseif ($this->node==MOD){
			(file_exists(VIEW.$this->url[0].'.'.EXT))? $this->template = $this->url[0] : $this->template = NONE;
		} elseif ($this->node==CONTENT){
			$concat = '.';
			if($obj->getHierarchy($this->url[0])==LISTING):
				(strlen(LISTING)>0)? $concat = '.' : $concat = '';
			endif;
			$this->template = $obj->getHierarchy($this->url[0]).$concat.$this->url[0];
		} else {
			$this->template = NONE;
		}
		
		$this->header = self::partial(HEADER);
		$this->header = self::partial(NAVIGATION);
		$this->footer = self::partial(FOOTER);
		return self::layout($this->template);
	}
	
	/** Partial method
	/*  Looks for partial files and loads them.  Otherwise it will assemble a default partial by auto-loading the files
	*/
	public function partial($arg){
		$output = '';
		if(file_exists(PARTIAL.$arg.'.'.EXT)) {
			require_once(PARTIAL.$arg.'.'.EXT);
		} elseif($arg==HEADER) {
			$output .= '<!DOCTYPE html><html lang="en"><head><title></title>'. "\xA"; //REFACTOR $this->obj->getTitle($url)
			$output	.= self::settingsPreload('css'). "\xA";
			$output	.= parent::extension(HEADER). "\xA"; // load the header extension file
			$output	.= parent::extension(META). "\xA";
			$output	.= '</head><body>'. "\xA";
			return $output;
		} elseif($arg==FOOTER) {
			$output .= '<footer>'. "\xA";
			$output .= parent::extension(FOOTER). "\xA"; // load the footer extension file
			$output .= self::settingsPreload('js'). "\xA";
			$output .= '</footer></body></html>';
			return $output;
		} else {
			return '<div class="error">'.parent::alerts('partial').'</div>';
		}
	}
	
	/** Settings Preload Method
	/*	 Auto-loads files for the partial method
	*/
	public function settingsPreload($arg,$contents=null){
		$result = '';
		$combined = array();
		if($arg=='css'){
			$open = "<link rel='stylesheet' type='text/css' ";
			$src = "href=";
			$close = " hreflang='en'>";
		} elseif($arg=='js'){
			$open = "<script type='text/javascript' ";
			$src = "src=";
			$close = "></script>";
		} else {
			$open = "<".$arg;
			$src = $contents." href=";
			$close = ">";
		}
		foreach ($this->$arg as $filename) {
				if(Helper::viewValidate($filename.'.css','',CSS)){
					$combined[] = $filename.'.css';
				}
		}
		$combined = array_unique(array_merge($combined,Helper::traversePrefix(CSS)));
		foreach ($combined as $filename){
			$result .= $open.$src."'".VIEW.$arg."/".$filename.".".$arg."'".$close. "\xA";
		}
		return $result;
	}
	
	/** Layout method
	/*  Renders the template and partials
	*/
	public function layout($arg){
		echo($this->header);
		echo($this->navigation);
		require_once(VIEW.$arg.'.'.EXT);
		echo($this->footer);
	}
	
}

?>