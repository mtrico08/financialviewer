<?php
namespace Core\Models;

class Modules implements Node {
	
	public $modules;
	
	//REACTOR:  Constructor Injection
	public function __construct($array){
		return $this->modules = self::getArray($array);
	}
	
	/** Get Array Method
	/*  This creates the array by traversing the directory and then removing the deactivated modules.  Used by getValid.
	/*  Modules are all automatically loaded unless indicated otherwise, so traversing the directory registers them.
	*/	
	public function getArray($array){
		$this->modules = scandir(MODULE);
		$this->modules = array_diff($this->modules,array_merge($array,array('..','.')));
		return $this->modules;
	}
	
	/** Get Valid Method
	/*  Compares URL from render controller against getArray, to validate a module
	/*  returns TRUE or FALSE
	*/
	public function getValid($url){
		return Helper::validate($url,$this->modules);
	}

	/** Abstract Methods
	/*  Necessary for building the subnodes
	/*
	*/
//	abstract public function setName();
//	abstract public function getName();
//	abstract public function alerts();
		//If a content type, module, or page name conflicts, we need to alert the administrator in the browser.
		//Content Type trumps Module (deactivates module).  Both trump page (deactivates page)

}

?>