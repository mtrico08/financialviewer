<?php
namespace Core\Models;

use \Core\Models as Model;
use \Core\Controllers\RenderController as Render;
use \Customize\Config\Settings as Config;

/* constants for directory naming conventions */
		/* REFACTOR:  This shouldn't be a constant.  The render controller should read it from the subnode's getname */
		define('TITLE','My Snazzy Framework'); // This is where we could load up the basic configuration settings from the DB
		/* END REFACTOR */
		define('DOMAIN',$_SERVER['HTTP_HOST']);
		//define('ROOT',$_SERVER['DOCUMENT_ROOT'].'/');
		define('ROOT',DOMAIN.'/../');
		define('MODEL','models/');
		define('CONTROLLER','controllers/');
		define('CORE',ROOT.'core/');
		define('CUSTOM',ROOT.'customize/');
		define('CONFIG',CUSTOM.'config/');
		define('SETTINGS',CONFIG.'settings.class.php');
		define('MODULE',ROOT.'modules/');
		define('VIEW',ROOT.'views/');
		define('PARTIAL',VIEW.'partials/');
		define('CSS',VIEW.'css/');
		define('IMG',VIEW.'img/');
		define('JS',VIEW.'js/');
		
/* constants for logic and file naming conventions */
		define('ARCHIVE','archive');
		define('LISTING','');
		define('SINGLE','single');
		define('CONTENT','content');
		define('MOD','module');
		define('PAGE','page');
		define('HOME','home'); // home template filename
		define('DEF','default'); // default template filename
		define('NONE','404'); // 404 template filename
		define('EXT','php'); // view file extension (yes, this is a bit unnecessary)
		define('HEADER','header'); // header partial filename
		define('FOOTER','footer'); // header partial filename
		define('META','meta'); // header extension meta filename
		define('NAVIGATION','menu'); // nav extension meta filename

Class Factory {
	
	protected $url;
	protected $page;
	protected $content;
	protected $module;
	protected $layout;
	
	public function __construct(){
	
		spl_autoload_extensions('.class.php');
		spl_autoload_register();
		
		$this->url = Helper::urlParser();
		
		// Load administrator settings
		$settings = new Model\Settings(new Config);
		// Load page node
		$this->page = new Model\Page($settings->getSitemap());
		// Load content node
		$this->content = new Model\Content($settings->getContent());
		// Load module node
		$this->module = new Model\Modules($settings->getModules());
		
		// Load rendering controller
		return $this->layout = new Render($this->url,self::objectDispatcher($this->url),$settings->getCss(),$settings->getJs());
		
	}
	
	/** Object Dispatcher
	/*  Find node type of URL request
	/*	Used by render controller to read a URL for directing which node model to validate against
	*/
	public function objectDispatcher($url){
		$result = '';
		if($this->module->getValid($url)){
			$result = MOD;
		} elseif($this->content->getValid($url)) {
			$result = CONTENT;
		} elseif($this->page->getValid($url)) {
			$result = PAGE;
		} else {
			$result = FALSE;
		}
		return $result;
	}
	
}

?>