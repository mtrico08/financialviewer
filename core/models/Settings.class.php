<?php
namespace Core\Models;

	/** Class Settings
	/*  Retrieves the administrator settings (which is essentially a weak controller).  
	/*  These settings will be used by all node models.  Models will check if true before using these settings.
	/*  With this class, we could easily change settings to XML, or a large array, or contained in the DB and simply update this class without interrupting the rest of the framework.
	/*  Getters and Setters are largely unnecessary versus accessing the public properties from the settings directly but adds a level of validation and reduces direct access to a less secure directory.
	/*  Security actions should be built into this class, to review potential hazards from "settings" as well.
	/*  Helper class methods can be used to sanitize the user's inputs in this class as well.
	*/

Class Settings {

	public $config;
	
	public function __construct(\Customize\Config\Settings $config){
		return $this->config = $config;
	}
	
	public function getSitemap(){
		return !empty($this->config->sitemap)? Helper::clean($this->config->sitemap) : FALSE;
	}
	
	public function getCss(){
		return !empty($this->config->css_order)? Helper::clean($this->config->css_order) : FALSE;
	}
	
	public function getJs(){
		return !empty($this->config->js_order)? Helper::clean($this->config->js_order) : FALSE;
	}
	
	public function getModules(){
		return !empty($this->config->deactivate_module)? Helper::clean($this->config->deactivate_module) : FALSE;
	}
	
	public function getContent(){
		return !empty($this->config->content_type)? Helper::clean($this->config->content_type) :  FALSE;
	}
}

?>