<?php
namespace Core\Models;

Class Content implements Node {
	
	public $content;
	
	//REACTOR:  Constructor Injection
	public function __construct($array){
		return $this->content = self::getArray($array);
	}
	
	/** Get Array Method
	/*  This creates the array by traversing the directory and then removing the deactivated content.  Used by getValid.
	/*  Content Types are not automatically loaded by traversing the folder.  They're registered in the settings.
	*/
	public function getArray($array){
		$this->content = array_keys($array);
		return $this->content;
	}
	
	/** Get Valid Method
	/*  Compares URL from render controller against getArray, to validate a module
	/*  returns TRUE or FALSE
	*/
	public function getValid($url){
		return (Helper::validate($url,$this->content)) ? Helper::viewValidate($url,CONTENT) : FALSE;
	}
	
	public function getHierarchy($url){
			if(array_key_exists(1,$url)&&strtolower($url[1]==ARCHIVE)){
				return ARCHIVE;
			}elseif(array_key_exists(1,$this->url)&&strlen($this->url[1])>=1){
				return SINGLE;
			}else{
				return LISTING;
			}
	}
	
	/** Abstract Methods
	/*  Necessary for building the sub-nodes
	*/
//	abstract public function setName();
//	abstract public function getName();
//	abstract public function alerts();
		//If a content type, module, or page name conflicts, we need to alert the administrator in the browser.
		//Content Type trumps Module (deactivates module).  Both trump page (deactivates page)

}

?>