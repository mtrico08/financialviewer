<?php
namespace Core\Models;

Class Render {
	
	/** Get Array Method
	/*  Intended to retrieve the JS and CSS settings
	*/

	/** Extension Method
	/*	Finds partial extentions
	*/
	public function extension($arg){
		$result = '';
		foreach (glob(PARTIAL.$arg.".inc.".EXT) as $filename)
		{
			$result .= file_get_contents($filename);
		}
		return $result;
	}
	
	/** Alerts Method
	/*  Returns alert for a missing partial or view... if validation fails everywhere else
	*/
	public function alerts($arg){
		return 'Sorry, this '.$arg.' is either not loading or non-existent';
	}

}

?>