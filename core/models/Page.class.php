<?php
namespace Core\Models;

class Page implements Node {
	
	public $page;
	
	//REACTOR:  Constructor Injection
	public function __construct($array){
		return $this->page = self::getArray($array);
	}
	
	/** Get Array Method
	/*  This creates the array by traversing the directory and then removing the deactivated page.  Used by getValid.
	/*  page are all automatically loaded unless indicated otherwise, so traversing the directory registers them.
	*/	
	public function getArray($array){
		$this->page = $array; // array_keys($array);
		return $this->page;
	}
	
	/** Get Valid Method
	/*  Compares URL from render controller against getArray, to validate a module
	/*  returns TRUE or FALSE
	*/
	public function getValid($url){
		return Helper::validate($url,array_merge($this->page,array('')));
	}

	public function getTitle($url){
		return $url;
	}

}

?>