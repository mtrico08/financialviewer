<?php
namespace Core\Models;

/** Class Helper
/*  This class contains a variety of global algorithms that may be used by models and controllers alike
/*  These methods are used throughout the framework and for that reason, are static
/*  As these methods are all static, there's an inherent flaw in dependency.  Cross dependency within helper is a required benefit
/*  REFACTOR:  Make this a parent class extended by all models (all controllers extend the models)
/*  It's a bad
*/
		
Class Helper {
	
	public function __construct(){
	}
	
	/** URL Parser
	/*  Creates Array of a URL argument and drops the trailing slash
	*/
	public static function urlParser($arg=null){
		$arg = (is_null($arg))? $_SERVER['REQUEST_URI'] : $arg;
		return Helper::clean(explode('/',ltrim($arg,'/')));
	}
	
	/** Namer Method
	/*	Builds Namespaces
	*/
	public static function namer(){
		$current = str_replace("/","\\",__DIR__);
		$cut = strlen(ROOT);
		$ns = substr($current,$cut);
		return $ns;
	}
	
	/** File Converter Method
	/*  Explodes filenames to an array to read and return the prefix to an array
	/*  May also take a second argument to return the file base name
	/*  Used primarily for modules but may also be used for content sub types
	*/
	public static function fileconverter($arg,$num=0){
		$result = self::clean(strtolower(explode('.',$arg)[$num]));
		return $result;
	}
	
	/** Traverse Prefix Method
	/*  Traverses the corresponding directory to build an array of file prefixes for auto-loading.
	/*  May also take a second argument to filter by the file base name
	/*  Used in modules, widgets, partials, and view files
	/*  Used as an argument by content types for validating archive, listing, or single
	/*  The coll parameter may be used to collect a different index in the array, instead of the prefix.
	*/
	public static function traversePrefix($directory,$base=null,$coll=0){
		$objects = scandir($directory,0);
		$results = array();
		if(!is_null($base)){
			foreach($objects as $object){
				if(self::fileconverter($object,1)==self::clean($base)):
					$results[] = self::fileconverter($object,$coll);
				endif;
			}
		}
		foreach($objects as $object){
			if(strlen(self::fileconverter($object))>0):
				$results[] = self::fileconverter($object);
			endif;
		}
		return $results;
	}

	/** Clean Method
	/*	Removes spaces to sanitize URLs, filenames, arrays and strings
	/*  Converts to lowercase for comparison operators
	*/	
	public static function clean($arg){
		$result = array();
		$search = array('%20',' ');
		if(!is_array($arg)){
			$result = str_replace($search,"_",strtolower($arg));
		} else {
			foreach($arg as $key=>$value){
				$result[self::clean($key)] = self::clean($value);
			}
		}
		return $result;
	}
	
	/** Validate Method
	/*  Checks if the URL input from a render controller is in the array of the model calling this method.
	/*  Standard use in model getValid
	/*  REFACTOR:  Only validates start of a URL, so single articles and other nested URL's may be an issue.
	*/	
	public static function validate($url,$array){
		if(is_array($url)){
			return (in_array(self::clean($url[0]),$array)) ? TRUE : FALSE;
		} else {
			return (in_array(self::clean($url),$array)) ? TRUE : FALSE;
		}
	}
	
	/** View Validate Method
	/*  Checks if the view file corresponding to a URL is present or missing.
	/*  Used in validation, after the validate method.
	/*  Content types are checked for the archive, single, and listing, as well as filename without prefix.
	*/
	public static function viewValidate($url,$type,$dir=VIEW){
		$array = array();
		if($type==CONTENT){
			$array = array_merge(self::traversePrefix($dir,$url),self::traversePrefix($dir,$url,1));
		} else {
			$array = self::traversePrefix($dir);		
		};
		return (in_array(self::clean($url),$array)) ? TRUE : FALSE;
	}
	
}

?>