<?php
namespace Customize\Config;

/** Administrator framework settings
/*  This is essentially a controller for data to the model.  It's intentionally a rudimentary constructor class, for ease of use by administrator 
*/

Class Settings {
	
	public $sitemap;
	public $css_order;
	public $js_order;
	public $deactivate_module;
	public $content_type;

	public function __construct(){
		$this->css_order = array(
			'style',
			'responsive',
		);
		$this->js_order = array(
			'scripts',
		);
		$this->sitemap = array(
			// List sitemap for standard pages
			0=>'LL',
			1=>'PRKR',
			2=>'MNKD',
			3=>'INO',
			4=>'NFLX',
			5=>'ISIS',
			6=>'DIS',
			7=>'SGEN',
			8=>'MSG',
			9=>'WWE',
			10=>'GOOG',
			11=>'AAPL',
			12=>'SWIR',
			13=>'ARIA',
			14=>'OPK',
			15=>'INXN',
			16=>'AMZN',
			17=>'FEYE',
			18=>'TWTR',
			19=>'MU',
			20=>'AVAV',
			21=>'SSYS',
			22=>'DDD',
			23=>'NVS',
			24=>'YHOO',
			25=>'VIGAX',
			26=>'VFICX',
			27=>'VFITX',
			28=>'VTSAX',
			29=>'MSFT',
		);
		// Mark a module as deactivated
		$this->deactivate_module = array(
		//	'yahoo',
			'instagram',
		);
		// Register content types with settings
		$this->content_type = array(
			'content 1' => array(
				'listing'=>FALSE,
			),
			'content 2' => array(
			),
			'post' => array(
				'archive'=>'2'
			),
		);
		return true;
	}	
}

?>