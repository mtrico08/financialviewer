README
=======

Core Framework
----------------

The core contains a model and controller for each node as well as the framework and rendering engine.  A node in this sense are content types, modules, widgets.
The index file will load the Factory class first, which is specially labeled with the "lib" file extension, to reduce unnecessarily overloading.
The Factory will define constants, instantiate the settings and the rendering controller, while loading the static helper methods.
The render controller's actions will indicate what node to load and instantiate the objects accordingly.


Models
----------------

Model files are in the directory and namespace "model" and have function with filename.  The word "model" doesn't appear in the filename nor class.
Each model will have an abstract class for setters and getters, which inherits

Controllers
----------------

The controller files are in the directory and namespace "controller" and must have controller labeled in the filename and classname.  The controllers are used to digest requests from the browser whether processing a URL, Posts, Gets, or tying in a string of actions between framework nodes.

Modules
----------------

Widgets
----------------

Content Types
---------------------

These are basically different types of hierarchical content, like a Drupal content type or a WordPress post type.  The view files are labeled as "single", "archive", or "listing".  The listing page may also be labeled without a prefix.  This adds a bit of complexity to the code validation, which you'll find out but it makes it easier for the administrator... in the smallest way haha.  The settings also permit the administrator to configure different prefixes... why?  I dunno, why the heck not.

Layout
----------------

The rendering engine controller belongs to the core directory and instructs a number of methods from the settings file, located in /customize/config

### Partials (Header and Footer included)

The body's layout file should not include the header and footer as those are handled by partials in the "views/partials" directory.  Partials may also be used to create other global includes such as sidebars and navigation.  To create a partial, simply create a filename in that corresponding directory.  Partials may be rendered by passing the filename (without the extension) to "$this->partial('filename')".

In the absence of a required partial (ie. header and footer), the engine will automatically assemble a header and a footer by auto-loading the files in your asset directories (ie. css and js) with the basic DOM structure.  The settings file permits you to choose the order in which these files load, via the "js_order" and "css_order" properties.

You may also choose to modify the engine's auto-header and auto-footer features by creating an extension file.  Extension files are a partial within a partial, labeled in the same fashion as partials but with the "inc.php" extension.  "Header.inc.php", for instance, will automatically load in the bottom half of the <head> tag, after auto-loading has completed.  Extensions may be rendered by passing the filename (without the extension) to $this->extension('filename').

### Body Templates

The body is assembled by files in the "views" root directory.  The standard 404, default, and home layout files are required for every project.  The settings class file (/customize/config/) includes a sitemap property which may be modified to indicate all valid page URL's.  Any page missing will return a 404, the homepage is already configured.

To create a template file, simple create a filename.php file where "filename" is the name of the URL which this file belongs.  For instance "domain.com/tester" would have filename "tester.php".  No body wrapper tags are necessary at the template level, simply the contents of the body would suffice.  Basic DOM elements such as head, body, and footer tags are located in the corresponding partials.

In the event an identified URL has no corresponding filename, it will assume the "default" file.